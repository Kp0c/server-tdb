﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlayerIO.GameLibrary;


namespace ServersideGameCode
{
    [RoomType("TDB main roomType")]
    public class GameCode : Game<Player>
    {
        //IMPORTANT!
        
        #region VARS
        
        //TODO: сделать в соответствии с БД
        public List<Enemy> Enemies = new List<Enemy>()
        {
            new Enemy("Goblin",0,5,2,1),
            new Enemy("Zombie",1,50,20,10),
            new Enemy("SuperEnemy",2,1,1,1),
        };
        
        public List<Tower> Towers = new List<Tower>()
        {
            new Tower("Wizard tower", 8),
            new Tower("Wooden tower", 16)
        };

        public List<string> TowersObjectID = new List<string>() { };
        
        readonly List<string> colors = new List<string>
        {
            "[000080]",
            "[008000]",
            "[FFFF00]",
            "[FF00FF]",
            "[800080]",
            "[800000]",
        };
        
        readonly Int32 timerCooldownTickInSeconds = 30;
        readonly Int32 timerCreepSendTickInSeconds = 1;

        readonly List<Player> players = new List<Player>();

        readonly Random random = new Random();

        #endregion
        
        public override void GameStarted()
        {
            PreloadPlayerObjects = true;
        }
        
        public override void GameClosed()
        {
            foreach (Player p in players)
            {
                p.Send("chat", "Комната закрыта");
            }
        }
        
        //..................................................TIMERS
        
        #region
        
        public void Update()
        {
            foreach (Player player in players)
            {
                Player localPlayer = player;
                localPlayer.Send("UpdateState", localPlayer.HP, localPlayer.Money, localPlayer.Income,
                    players.Find(pl => pl != localPlayer).HP);
            }
        }
        
        public void Tick()
        {
            foreach (Player player in players)
            {
                player.Money += player.Income;
            }
        }
        
        public void SendCreep()
        {
            foreach (Player player in players)
            {
                if (player.CreepsToSend.Count > 0)
                {
                    Broadcast(player.CreepsToSend[0]);
                    player.CreepsToSend.RemoveAt(0);
                }
            }
        }
        
        #endregion
        
        //.......................................................END
        
        public override void UserJoined(Player player)
        {
            player.Send("YourID", player.Id);
            players.Add(player);
            if (players.Count <= 2)
            {
                player.Color = colors[random.Next(colors.Count)];
                colors.Remove(player.Color);
            }
            if (players.Count == 2)
            {
                InitGame();
            }
            if (players.Count > 2)
            {
                players[players.Count - 1].Send("DisconnectMuchPlayers");
                players[players.Count - 1].Disconnect();
            }

            PlayerIO.BigDB.Load("Users", player.ConnectUserId, delegate(DatabaseObject result)
            {
                player.Name = result.GetString("username");
            });
            
            Broadcast("chat", "привет, " + player.Name);
        }

        void InitGame()
        {
            Broadcast("chat", "Server say: oк, комната собрана");

            AddTimer(Update, 1000);
            AddTimer(Tick, timerCooldownTickInSeconds * 1000);
            AddTimer(SendCreep, timerCreepSendTickInSeconds * 1000);

                PlayerIO.BigDB.LoadRange("Enemies", "ID", null, null, null, 1000,
                    delegate(DatabaseObject[] objects)
                    {
                        Enemy temp;
                        foreach (DatabaseObject obj in objects)
                        {
                            temp = Enemies.Find(enemy => enemy.ID == obj.GetInt("ID"));
                            temp.Name = obj.GetString("Name");
                            temp.Price = obj.GetInt("Price");
                            temp.Bounty = obj.GetInt("Bounty");
                            temp.Income = obj.GetInt("Income");
                        }
                    },
                    delegate (PlayerIOError error){
                        PlayerIO.ErrorLog.WriteError("Ошибка в GameCode.cs", "Ошибка при получении врагов с БД",
                            "GameCode.cs строка 137 (обьявление ошибки - 155-156)", null);
                    });
        }
        
        public override void UserLeft(Player player)
        {
            players.Remove(player);
            Broadcast("chat", "Server say: Bye, " + player.Name);
            if (players.Count == 1)
            {
                players[0].Send("chat", "ваш противник вышел, вам засчитан тех. вин");
                Broadcast("Loose", player.Name);
                ForEachPlayer(p => p.Disconnect());
            }
        }
        
        public override void GotMessage(Player player, Message message)
        {
            if (players.Count == 2)
            {
                switch (message.Type)
                {
                    case "chat":
                        Console.WriteLine(player.Name + ": " + message.GetString(0));
                        Broadcast("chat", player.Color + player.Name + "[-]" +
                                          ": " + message.GetString(0));
                        break;
                    case "addTower":
                        if (player.Money >= Towers[message.GetInt(0)].Price)
                        {
                            player.Money -= Towers[message.GetInt(0)].Price;
                            Broadcast("addTower", /*player.Id,*/ message.GetInt(0), message.GetInt(1), message.GetInt(2));
                        }
                        else
                        {
                            player.Send("error", "у вас не хватит золота на это");
                        }
                        break;
                    case "reduceHP":
                        player.HP -= message.GetInt(0);
                        if (player.HP <= 0)
                            Broadcast("Loose", player.Name);
                        break;
                    case "sendEnemy":
                        if (player.Money >= Enemies[message.GetInt(0)].Price)
                        {
                            player.Money -= Enemies[message.GetInt(0)].Price;
                            player.Income += Enemies[message.GetInt(0)].Income;
                            //Broadcast("getEnemy", player.Name, message.GetInt(0));
                            player.CreepsToSend.Add(Message.Create("sendEnemy", player.Id, message.GetInt(0)));
                        }
                        else
                        {
                            player.Send("error", "у вас не хватит золота на это");
                        }
                        break;
                    case "KillCreep":
                        player.Money += Enemies[message.GetInt(0)].Bounty;
                        break;
                    default:
                        Console.WriteLine("Неизвестный тип команды: " + message.Type);
                        break;
                }
            }
            else
                player.Send("error", "Дождитесь второго игрока");
        }
    }
}
