namespace ServersideGameCode
{
    public class Enemy
    {
        public string Name = "Enemy";
        public int ID = 0;
        public int Price = 5;
        public int Bounty = 2;
        public int Income = 1;

        public Enemy(string name,int id, int price, int bounty, int income)
        {
            this.ID = id;
            this.Name = name;
            this.Price = price;
            this.Bounty = bounty;
            this.Income = income;
        }
    }
}