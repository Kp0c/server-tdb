﻿using System;
using System.Linq;

namespace ServersideGameCode
{
    public class Tower
    {
        public string Name = "Tower";
        public int Price = 8;

        public Tower(string name, int price)
        {
            this.Name = name;
            this.Price = price;
        }
    }
}
