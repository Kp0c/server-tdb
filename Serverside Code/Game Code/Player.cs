using System.Collections.Generic;
using PlayerIO.GameLibrary;

namespace ServersideGameCode
{
    public class Player : BasePlayer
    {
        public string Name;
        public int HP = 100;
        public int Money = 65;
        public int Income = 35;
        public string Color;
        public List<Message> CreepsToSend = new List<Message>();
    }
}